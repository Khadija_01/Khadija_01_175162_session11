/**
 * Created by Web App Develop-PHP on 8/19/2017.*/

$ (document).ready(function () {
    $("#btn").click(function () {
        var myRequest = new XMLHttpRequest();

        myRequest.open('GET','https://learnwebcode.github.io/json-example/animals-1.json');
        myRequest.onload = function () {
            var myData = myRequest.responseText;
            $("#display").html(myData);
            $("#display").css("background", "GREEN");
        };
        myRequest.send();
    });
});